import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Word {
	private Map<String,Integer> maps = new HashMap<String, Integer>();	
	//private int word_id;
	//private int word_frequence;
	private String genre;
	
	public Word(Map<String,Integer>maps, String genre){
		this.setMaps(maps);
		this.genre=genre;
	}

	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Map<String,Integer> getMaps() {
		return maps;
	}

	public void setMaps(Map<String,Integer> maps) {
		this.maps = maps;
	}



}
