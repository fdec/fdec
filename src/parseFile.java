import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

public class parseFile {

	public static void main(String[] args) throws IOException {

		String file = "/home/kejji/Téléchargements/bow.csv";
		BufferedReader br = null;
		String line = "";

		String splitter = ";";
		ArrayList<Word> songs = new ArrayList<>();
		ArrayList<Word> songsToWrite = new ArrayList<>();
		// Word songs[] = new Word[word.length] ;

		ArrayList<Map<String, String>> mapss = new ArrayList<Map<String, String>>();

		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			while ((line = br.readLine()) != null) {
				Map<String, Integer> maps = new HashMap<String, Integer>();

				String splitter2 = ":";
				String[] word = line.split(splitter);
				// System.out.println(word[0]);

				// songs.setGenre((word[word.length-1]).toString());
				// System.out.println(word.length);
				for (int i = 0; i < word.length - 1; i++) {
					// System.out.println(word[i]);
					String[] bis = word[i].split(splitter2);

					maps.put(bis[0], Integer.parseInt(bis[1]));

				}
				// System.out.println("word  :  "+word.length);
				// System.out.println(maps.size());
				Word song = new Word(maps, word[word.length - 1]);
				songs.add(song);
				// System.out.println(song.getGenre());

			}
			// System.out.println(songs.size());

			for (int i = 0; i < songs.size(); i++) {
				// System.out.println(songs.get(i).getMaps());
				Map<String, Integer> sortedMap = sortByComparator(songs.get(i)
						.getMaps());
				songs.get(i).setMaps(sortedMap);
			}

			/*
			 * System.out.println(songs.get(2).getMaps());
			 * System.out.println(songs.get(2).getGenre());
			 */
			// Map<String, Integer> sortedMap =
			// sortByComparator(songs.get(2).getMaps());
			// System.out.println(sortedMap);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		writeSongs("test.txt", songs);
		// System.out.println(songs.get(songs.size()-1).getMaps().keySet());

	}

	// //
	public static void writeSongs(String file, ArrayList<Word> songs)
			throws IOException {

		String eol = System.getProperty("line.separator");
		try (Writer writer = new FileWriter(file)) {
			for (int i = 0; i < songs.size(); i++) {

				// if(songs.get(i).getMaps().size()<40){
				System.out.println(songs.get(i).getMaps().size());

				// }
				// else{
				// for(int j=0;j<40;j++){
				int count = 0;
				for (Entry<String, Integer> entry : songs.get(i).getMaps()
						.entrySet()) {
					if(count<20) {
						writer.append(entry.getKey());
						count++;
					//writer.append();
					writer.append(" ");
					}
					else {
						break;
					}
					
				}
				// }
				writer.append(" -1 ");
				writer.append(songs.get(i).getGenre());
				writer.append(" -1 -2"+"\n");

				// }

			}
		} catch (IOException ex) {
			ex.printStackTrace(System.err);
		}

	}

	// /////
	private static Map<String, Integer> sortByComparator(
			Map<String, Integer> unsortMap) {

		// Convert Map to List
		List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(
				unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
					Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		// Convert sorted map back to a Map
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it
				.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	// ////

}
